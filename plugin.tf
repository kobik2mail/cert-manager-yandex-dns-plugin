### SA ###
############## Создание sa ##############
resource "yandex_iam_service_account" "sa-plugin" {
  name        = "sa-plugin"
  description = "service account"
  folder_id   = var.folder_id
}
############## Назначение прав ##############
resource "yandex_resourcemanager_folder_iam_binding" "sa-plugin" {
  folder_id = var.folder_id
  role      = "dns.editor"
  members   = [
    "serviceAccount:${yandex_iam_service_account.sa-plugin.id}",
  ]
}
############## Создание стат ключа ##############
resource "yandex_iam_service_account_static_access_key" "stat-key" {
  service_account_id = yandex_iam_service_account.sa-plugin.id
  description        = "static access key"
}
### SA ###
resource "helm_release" "cert_manager_webhook_yandex" {
  name       = "cert-manager-webhook-yandex"
  namespace  = var.namespace
  repository = local.helm_repository
  chart      = local.helm_chart
  version    = var.chart_version

}

resource "kubernetes_secret" "cert_manager_secret" {
  metadata {
    name      = "cert-manager-secret"
    namespace = var.namespace
  }

  data = {
    "key.json" = yandex_iam_service_account_static_access_key.stat-key.secret_key
  }
}

############## Создание ClusterIssuer ##############
resource "kubectl_manifest" "clusterissuer" {
  yaml_body = yamlencode(local.cluster_issuer)
}

