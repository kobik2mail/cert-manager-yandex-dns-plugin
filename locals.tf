locals {
  helm_chart      = "cert-manager-webhook-yandex"
  helm_repository = "./deploy/"
  cluster_issuer = {
    apiVersion = "cert-manager.io/v1"
    kind       = "ClusterIssuer"
    metadata = {
      name = "clusterissuer"
    }
    spec = {
      acme = {
        server = var.acme_server
        email  = var.acme_email
        privateKeySecretRef = {
          name = "secret-ref"
        }
        solvers = [{
          dns01 = {
            webhook = {
              config = {
                folder = var.folder_id
                serviceAccountSecretRef = {
                  name = "cert-manager-secret"
                  key  = "key.json"
                }
              }
              groupName = "acme.cloud.yandex.com"
              solverName = "yandex-cloud-dns"
            }
          }
        }]
      }
    }
  }
}

