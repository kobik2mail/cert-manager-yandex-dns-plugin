variable "namespace" {
  description = "Namespace для установки чарта"
  type        = string
  default     = "cert-manager"
}

variable "chart_version" {
  description = "Версия Helm чарта"
  type        = string
  default     = "1.0.8-1"
}

variable "acme_email" {
  description = "Email"
  type        = string
}

variable "acme_server" {
  description = "ACME сервер"
  type        = string
  default     = "https://acme-staging-v02.api.letsencrypt.org/directory"
}

variable "folder_id" {
  description = "ID папки в Yandex Cloud, где находится DNS зона"
  type        = string
}

variable "dns_zone" {
  description = "DNS"
  type        = string
}

variable "k8s_host" {
  type = string
  default = ""
}

variable "cluster_ca_certificate" {
  type = string
  default = ""
}

variable "k8s_exec" {
  type = object({
    api_version = string
    command = string
    args = list(string)
  })
}

variable "wait" {
  description = "Will wait until all resources are in a ready state"
  type        = bool
  default     = true
}

variable "timeout" {
  type        = number
  description = "Time in seconds to wait for any individual kubernetes operation"
  default     = 300
}